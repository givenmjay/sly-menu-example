jQuery(function($) {
	'use strict';

	// -------------------------------------------------------------
	// TOP MENU
	// -------------------------------------------------------------
	(function() {
		var $frame = $('#top-menu');
		var $wrap = $frame.parent().parent().parent();

		var $frameOne = $('#sub-menu');
		var $wrapOne = $frameOne.parent().parent().parent();

		// Call Sly on frame
		$frame.sly({
			horizontal : 1,
			itemNav : 'basic',
			smart : 1,
			activateOn : 'click',
			mouseDragging : 1,
			touchDragging : 1,
			releaseSwing : 1,
			startAt : 0,
			scrollBar : $wrap.find('.scrollbar'),
			scrollBy : 1,
			pagesBar : $wrap.find('.pages'),
			activatePageOn : 'click',
			speed : 300,
			elasticBounds : 1,
			easing : 'easeOutExpo',
			dragHandle : 1,
			dynamicHandle : 1,
			clickBar : 1,

			// Cycling
			cycleBy : 'pages',
			cycleInterval : 1000,
			pauseOnHover : 1,
			startPaused : 1,

			// Buttons
			prevPage : $wrap.find('.prevPage'),
			nextPage : $wrap.find('.nextPage')
		});

		// Pause button
		$wrap.find('.pause').on('click', function() {
			$frame.sly('pause');
		});

		// Resume button
		$wrap.find('.resume').on('click', function() {
			$frame.sly('resume');
		});

		// Toggle button
		$wrap.find('.toggle').on('click', function() {
			$frame.sly('toggle');
		});

		// -------------------------------------------------------------
		// SUB MENU
		// -------------------------------------------------------------

		// Call Sly on frame
		$frameOne.sly({
			horizontal : 1,
			itemNav : 'basic',
			smart : 1,
			activateOn : 'click',
			mouseDragging : 1,
			touchDragging : 1,
			releaseSwing : 1,
			startAt : 0,
			scrollBar : $wrapOne.find('.scrollbar'),
			scrollBy : 1,
			pagesBar : $wrapOne.find('.pages'),
			activatePageOn : 'click',
			speed : 300,
			elasticBounds : 1,
			easing : 'easeOutExpo',
			dragHandle : 1,
			dynamicHandle : 1,
			clickBar : 1,

			// Cycling
			cycleBy : 'pages',
			cycleInterval : 1000,
			pauseOnHover : 1,
			startPaused : 1,

			// Buttons
			prevPage : $wrapOne.find('.prevPage'),
			nextPage : $wrapOne.find('.nextPage')
		});

		// Pause button
		$wrapOne.find('.pause').on('click', function() {
			$frameOne.sly('pause');
		});

		// Resume button
		$wrapOne.find('.resume').on('click', function() {
			$frameOne.sly('resume');
		});

		// Toggle button
		$wrapOne.find('.toggle').on('click', function() {
			$frameOne.sly('toggle');
		});

	}());

});